from __future__ import unicode_literals

from django.db import models

class Tweetdata(models.Model):
	text = models.TextField()
	user = models.CharField(max_length=10)

	def __str__(self):
		return self.text