from rest_framework import serializers
from .models import Tweetdata

class TweetdataSerializer(serializers.ModelSerializer):

	class Meta:
		model = Tweetdata
		fields = ('text','user')
		